package com.hcl.customer.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.hcl.customer.entity.Customer;
import com.hcl.customer.service.ICustomerService;

@Controller
public class CustomerController {

	@Autowired
	private ICustomerService customerService;

	@GetMapping("/addCustomer")
	public String addCustomer() {
		return "addCustomer";
	}

	@PostMapping("/register")
	public String customerRegistration(@ModelAttribute Customer customer, HttpSession session) {
		System.out.println(customer);
		customerService.addCustomer(customer);
		session.setAttribute("msg", "Customer details added successfully");
		return "redirect:/";

	}

	@GetMapping("/get")
	public String home(Model model) {
		List<Customer> customer = customerService.getAllCustomers();
		model.addAttribute("customer", customer);
		return "index";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable int id, Model model) {
		Customer customer = customerService.getCustomerById(id);
		model.addAttribute("customer", customer);
		return "editCustomer";
	}

	@PostMapping("/update")
	public String updateCustomer(@ModelAttribute Customer customer, HttpSession session) {
		customerService.addCustomer(customer);
		System.out.println(customer);
		session.setAttribute("msg", "Customer details updated Successfully");
		return "redirect:/";

	}

	@GetMapping("/delete/{id}")
	public String deleteCustomer(@PathVariable int id, HttpSession session) {
		System.out.println("Customer with id " + id + " deleted");
		customerService.deleteCustomer(id);
		session.setAttribute("msg", "Customer deleted");
		return "redirect:/";
	}

}
