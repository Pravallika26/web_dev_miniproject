package com.hcl.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.customer.entity.Customer;

@Repository
public interface ICustomerRepository extends JpaRepository<Customer, Integer>{

}
