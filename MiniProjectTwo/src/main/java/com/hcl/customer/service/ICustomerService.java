package com.hcl.customer.service;

import java.util.List;

import com.hcl.customer.entity.Customer;

public interface ICustomerService {

	public Customer addCustomer(Customer customer);
	
	public List<Customer> getAllCustomers();
	
	public Customer getCustomerById(int id);
	
	public Customer updateCustomer(Customer customer);
	
	public void deleteCustomer(int id);
}
