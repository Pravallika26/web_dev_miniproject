package com.hcl.customer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.customer.entity.Customer;
import com.hcl.customer.repository.ICustomerRepository;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	public ICustomerRepository customerRepo;

	@Override
	public Customer addCustomer(Customer customer) {
		return customerRepo.save(customer);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return customerRepo.findAll();
	}

	@Override
	public Customer getCustomerById(int id) {
		Optional<Customer> optCust = customerRepo.findById(id);
		Customer customer = null;
		if (optCust.isPresent()) {
			customer = optCust.get();
		} else {
			throw new RuntimeException("Customer with id " + id + " not found");
		}
		return customer;
	}

	@Override
	public Customer updateCustomer(Customer customer) {
		return customerRepo.save(customer);
	}

	@Override
	public void deleteCustomer(int id) {
		System.out.println("Customer with id " + id + " deleted");
		customerRepo.deleteById(id);
	}

}
