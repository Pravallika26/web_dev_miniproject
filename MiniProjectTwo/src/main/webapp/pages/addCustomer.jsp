<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body style="background: white; color: black;">



	<div style="color: white; background: green; height: 80px"
		class="topic">
		<center>
			<br>

			<h1 style="font-size: 2vw">CUSTOMER RELATIONSHIP MANAGEMENT</h1>
		</center>
	</div>
	<a class="active" href="/"><button>Back to homepage</button></a>

	<h3>Save Customer</h3>

	<form style="background-color: white" class="modal-content animate"
		action="/register" method="post">

		<div class="form-group">

			<label for="firstname"><b>First Name: </b></label> <input type="text"
				placeholder="Enter firstname" name="firstName" required><br>

			<label for="lastname"><b>Last Name: </b></label> <input type="text"
				placeholder="Enter lastname" name="lastName" required><br>

			<label for="Email"><b>Email: </b></label> <input type="text"
				placeholder="Enter Email" name="email" required> <br> <br>
			<button class="btn btn-success" type="submit">Save</button>

		</div>

	</form>

</body>
</html>