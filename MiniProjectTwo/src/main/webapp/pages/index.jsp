<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Relationship Management</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>



<div style="color: black; background: green; height: 80px" class="topic">
	<center>
		<br>

		<h1 style="font-size: 2vw">CUSTOMER RELATIONSHIP MANAGEMENT</h1>
	</center>
</div>


<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
	<a class="active" href="/"><i class="fa fa-fw fa-home"></i> HOME</a> <a
		href="/get"><i class="fa fa-fw fa-search"></i> Display Customers</a> <a
		href="addCustomer"><i class="fa fa-fw fa-user"></i> Add Customer</a>
</nav>

</div>


<p><%=session.getAttribute("msg")%></p>


<div class="table_responsive">
	<table border="1" width="50%" cellpadding="2" align="center">

		<thead>
			<tr align="center">

				<th><b>First Name</b></th>
				<th><b>Last Name</b></th>
				<th><b>Email</b></th>
				<th colspan="2"><b>Action</b></th>

			</tr>
		</thead>


		<c:forEach var="i" items="${customer}">
			<tr align="center">

				<td><c:out value="${i.getFirstName() }"></c:out></td>
				<td><c:out value="${i.getLastName() }"></c:out></td>
				<td><c:out value="${i.getEmail() }"></c:out></td>
				<td colspan="2"><a href="edit/${i.getId()}"><button
							class="btn btn-link btn-sm">UPDATE</button></a> 
					<a href="delete/${i.getId()}">
						<button class="btn btn-link btn-sm">DELETE</button>
			</tr>
		</c:forEach>

	</table>
</div>

</body>
</html>