<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>

	<div style="color: white; background: green; height: 80px"
		class="topic">
		<center>
			<br>

			<h1 style="font-size: 2vw">CUSTOMER RELATIONSHIP MANAGEMENT</h1>
		</center>
	</div>


	<div class="navbar">
		<a class="active" href="/"><button>Back to homepage</button></a><br>
	</div>


	<h2>Update Customer</h2>


	<form class="modal-content animate" action="/update" method="post">


		<div class="container">

			<input type="hidden" value="${id}" name="id"> <label
				for="firstname"><b>First Name: </b></label> <input type="text"
				placeholder="Enter firstname" name="firstName" required><br>

			<label for="lastname"><b>Last Name: </b></label> <input type="text"
				placeholder="Enter lastname" name="lastName" required><br>

			<label for="Email"><b>Email: </b></label> <input type="text"
				placeholder="Enter Email" name="email" required><br> <br>
			<button class="btn btn-success " type="submit">Update</button>

		</div>

	</form>

</body>
</html>
